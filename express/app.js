const express = require('express');
const app = express();
const port = 3000;

const os = require("os");
const hostname = os.hostname();

app.get('/api', (req, res) => {
  return res.send(`Hello World! Express ${hostname} cambio 2`);
})

app.listen(port);